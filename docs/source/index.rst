Welcome to Image Selection AV RGBI's documentation!
===================================================

select or stitch the best image for each uplot and save it in "selected_images" directory

.. toctree::
	:maxdepth: 2
	:caption: Contents

	quickstart
	cli
	qualityassessment

.. toctree::
	:maxdepth: 2
	:caption: Specifications

	inputformats
	outputformats

.. toctree::
	:maxdepth: 3
	:caption: API Reference

	api/imageselectionavrgbi
