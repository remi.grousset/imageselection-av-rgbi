"""
Command line interface for the Image Selection AV RGBI module.
"""
import argparse
import logging
import sys

import imageselectionavrgbi
from . import cliutils
from pathlib import Path

from .image_picker_drone import image_selection


_LOGGER = logging.getLogger(__name__)


def create_parser():
	"""
	Create the parser for the imageselection-av-rgbi command.

	:return: Configured parser.
	:rtype: argparse.ArgumentParser
	"""
	parser = argparse.ArgumentParser(
		description='''
select or stitch the best image for each uplot and save it in "selected_images" directory
'''
		)
	parser.add_argument(
		'-v', '--version',
		action='version',
		version='%(prog)s v{}'.format(imageselectionavrgbi.__version__)
		)

	# Positional arguments, declaration order is important
	parser.add_argument(
		'input_arg',
		type=cliutils.sanitize_path,
		help='either the directory where to find the images processed with Phenoscript, either the orthomosaic'
		)

	parser.add_argument(
		'attachment',
		type=cliutils.sanitize_path,
		help='if the extracted plot directory ==> "extracted_plot_data.csv"; if the orthomosaic ==> the geojson file with the GPS coodinates of the plots'
		)
	# Optional arguments
	cliutils.add_boolean_flag(parser, 'debug', 'Enable debug outputs. Imply --verbose.')
	cliutils.add_boolean_flag(parser, 'verbose', 'Enable debug logging.')

	return parser

def main(args=None):
	"""
	Run the main procedure.

	:param list args: List of arguments for the command line interface. If not set, arguments are
		taken from ``sys.argv``.
	"""
	parser = create_parser()
	args = parser.parse_args(args)
	args.verbose = args.verbose or args.debug

	# Ensure the directory exists to create the log file
	work_dir = args.input_arg
	if 'Extracted-Plots' in work_dir.name:
		output_folder = args.input_arg.joinpath('delivery_package')
	elif "orthomosaic" in work_dir.name:
		output_folder = args.input_arg.parent.joinpath('delivery_package')
	else:
		output_folder = args.input_arg.parent.joinpath('delivery_package')

	output_folder.mkdir(parents=True, exist_ok=True)
	log_folder = output_folder.joinpath('logs')
	log_folder.mkdir(parents=True, exist_ok=True)
	log_filename = log_folder.joinpath('imageselection.log')

	cliutils.setup_logging(debug=args.verbose, filename=log_filename)
	_LOGGER.debug('command: %s', ' '.join(sys.argv))
	_LOGGER.debug('version: %s', imageselectionavrgbi.__version__)

	# Call the main function of the module
	image_selection(work_dir, args.attachment)

if __name__ == '__main__':
	main()
