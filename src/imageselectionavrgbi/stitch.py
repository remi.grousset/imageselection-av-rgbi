import cv2
import matplotlib.pyplot as plt
import numpy as np
from skimage.filters import threshold_otsu


def remove_black_stripes(img):
    """
    crop the input image to minimize the black pixels around the microplot. Return the cropped image
    :param img: ndarray containing the image that needed to be cropped
    :return crop: ndarray containing the stitched image
    """
    black_edges = np.zeros((img.shape[0] + 200, img.shape[1] + 200, img.shape[2]))
    black_edges[100: 100 + img.shape[0], 100: 100 + img.shape[1], :] = img
    img = black_edges.astype(np.uint8)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    _,thresh = cv2.threshold(gray,1,255,cv2.THRESH_BINARY)
    # plt.imshow(thresh)
    # plt.show()
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    max = 0
    for i in contours:
        if len(i) > max:
            max = len(i)
            cnt = i
    
    x,y,w,h = cv2.boundingRect(cnt)
    crop = img[y:y+h,x:x+w]
    return crop


def stitch(img2, img1):
    """stitch two images containing each one an extremity of the plot
    :param img1: ndarray containing the image of the left extremity of the plot
    :param img2: ndarray containing the image of the right extremity of the plot
    return result: ndarray containing the image stitched with the two input images
    """
    # ********************** mettre un if la rotation est nécessaire (sounds useless)
    img2 = remove_black_stripes(img2)
    sift1 = cv2.SIFT_create()
    (kp1, features1) = sift1.detectAndCompute(img1, None)
    kp1 = np.float32([kp.pt for kp in kp1])

    sift2 = cv2.SIFT_create()
    (kp2, features2) = sift2.detectAndCompute(img2, None)
    kp2 = np.float32([kp.pt for kp in kp2])

    matcher = cv2.DescriptorMatcher_create("BruteForce")
    rawMatches = matcher.knnMatch(features1, features2, 2)
    matches = []
    ratio = 0.75
    reprojThresh = 10.0
    for m in rawMatches:
        if len(m) == 2 and m[0].distance < m[1].distance * ratio:
            matches.append((m[0].trainIdx, m[0].queryIdx))

    # computing a homography requires at least 4 matches
    if len(matches) > 4:
        # construct the two sets of points
        ptsA = np.float32([kp1[i] for (_, i) in matches])
        ptsB = np.float32([kp2[i] for (i, _) in matches])

        # compute the homography between the two sets of points
        (H, status) = cv2.findHomography(ptsA, ptsB, cv2.RANSAC,
            reprojThresh)

    # **** gestion d'erreur sur le H *****
    result = cv2.warpPerspective(img1, H, (int(img1.shape[1]*1.5), int(img1.shape[0] * 1.5)))
    result[0:img2.shape[0], 0:int(img2.shape[1] / 2)] = img2[:, :int(img2.shape[1] / 2)]

    result = remove_black_stripes(result)
    return result
